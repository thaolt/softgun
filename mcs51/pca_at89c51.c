/**
 ************************************************************
 * Atmel AT89C51 Programmable Counter Array (PCA)
 ************************************************************
 */

#define REG_CMOD	(0xD9)
#define REG_CCON	(0xD8)
#define REG_CCAP0H	(0xFA)
#define REG_CCAP1H	(0xFB)
#define REG_CCAP2H	(0xFC)
#define REG_CCAP3H	(0xFD)
#define REG_CCAP4H	(0xFE)
#define REG_CCAP0L	(0xEA)
#define REG_CCAP1L	(0xEB)
#define REG_CCAP2L	(0xEC)
#define REG_CCAP3L	(0xED)
#define REG_CCAP4L	(0xEE)
#define REG_CCAPM0	(0xDA)
#define REG_CCAPM1	(0xDB)
#define REG_CCAPM2	(0xDC)
#define REG_CCAPM3	(0xDD)
#define REG_CCAPM4	(0xDE)
#define REG_CH		(0xF9)
#define REG_CL		(0xE9)
