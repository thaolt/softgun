/*
 ************************************************************************************************
 * RX63-Flash
 *      Emulation of the internal flash of Renesas RX63 series
 ************************************************************************************************
 */

#include "bus.h"
BusDevice * RX63nFlash_New(const char *flash_name);
