/**
 **************************************************
 * LVDAa Low voltage detection circuit of RX111
 **************************************************
 */

#include "lvda_rx111.h"
#include "bus.h"

#define REG_LVD1CR1(base)   (0x800e0)
#define REG_LVD1SR(base)    (0x800e1)
#define REG_LVD2CR1(base)   (0x800e2)
#define REG_LVD2SR(base)    (0x800e3)
#define REG_LVCMPCR(base)   (0x8c297)
#define REG_LVDLVLR(base)   (0x8c298)
#define REG_LVD1CR0(base)   (0x8c29a)
#define REG_LVD2CR0(base)   (0x8c29b)

typedef struct LVDAa {
    BusDevice bdev;
    uint8_t regLVD1CR1;
    uint8_t regLVD1SR;
    uint8_t regLVD2CR1;
    uint8_t regLVD2SR;
    uint8_t regLVCMPCR;
    uint8_t regLVDLVLR;
    uint8_t regLVD1CR0;
    uint8_t regLVD2CR0;
} LVDAa;

/**
 **************************************************
 * Constructor for Low voltage detection circuit.
 **************************************************
 */
void
RX111LVDAa_New(const char *name)
{

} 
