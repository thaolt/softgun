/*
 ************************************************************************************************
 * RX65-Flash
 *      Emulation of the internal flash of Renesas RX65  series
 ************************************************************************************************
 */
#include "bus.h"
BusDevice * RX65Flash_New(const char *flash_name);
