SRCFILES += rx/i2c_rx62t.c rx/idecode_rx.c rx/instructions_rx.c rx/cpu_rx.c \
	    rx/usb_rx.c rx/flash_rx.c rx/icu_rx62n.c rx/sci_rx62n.c rx/cmt_rx62n.c \
	    rx/ioport_rx62n.c rx/clk_rx62n.c rx/etherc_rx62n.c rx/crc_rx.c \
	    rx/wdt_rx62n.c rx/clk_rx63n.c rx/ioport_rx63n.c rx/wdta_rx63n.c \
	    rx/rtc_rx63n.c rx/s12ada_rx63n.c rx/flash_rx63n.c rx/can_rx63n.c rx/rspi_rx62n.c \
		rx/clk_rx111.c rx/flash_rx111.c rx/iwdt_rx111.c rx/ada_rx.c \
		rx/da_rx63.c rx/adb_rx.c rx/reset_rx63.c rx/mpc_rx63.c rx/mtu2a.c rx/mpc_rx111.c \
		rx/poe2a.c rx/clk_rx65.c rx/icu_rx65.c rx/grpirq_rx65.c rx/scint_rx65.c rx/flash_rx65.c
