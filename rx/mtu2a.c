/*
 **********************************************************************************************
 * Renesas MTU2a Timer module  
 *
 * State: not implemented 
 *
 * Copyright 2015 Jochen Karrer. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are
 * permitted provided that the following conditions are met:
 *
 *   1. Redistributions of source code must retain the above copyright notice, this list of
 *       conditions and the following disclaimer.
 *
 *    2. Redistributions in binary form must reproduce the above copyright notice, this list
 *       of conditions and the following disclaimer in the documentation and/or other materials
 *       provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY Jochen Karrer ``AS IS'' AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 * FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * The views and conclusions contained in the software and documentation are those of the
 * authors and should not be interpreted as representing official policies, either expressed
 * or implied, of Jochen Karrer.
 *
 **********************************************************************************************
 */

#include <stdint.h>
#include <inttypes.h>
#include "bus.h"
#include "sglib.h"
#include "sgstring.h"
#include "clock.h"
#include "cycletimer.h"
#include "signode.h"
#include "mtu2a.h"

/* base is 0x88600 */
#define REG_MTU0_TCR(base)      ((base) + 0x100)
#define REG_MTU1_TCR(base)      ((base) + 0x180)
#define REG_MTU2_TCR(base)      ((base) + 0x200)
#define REG_MTU3_TCR(base)      ((base) + 0x00)
#define REG_MTU4_TCR(base)      ((base) + 0x01)
#define REG_MTU5_TCRU(base)     ((base) + 0x284)
#define REG_MTU5_TCRV(base)     ((base) + 0x294)
#define REG_MTU5_TCRW(base)     ((base) + 0x2a4)
#define REG_MTU0_TMDR(base)     ((base) + 0x101)
#define REG_MTU1_TMDR(base)     ((base) + 0x181)
#define REG_MTU2_TMDR(base)     ((base) + 0x201)
#define REG_MTU3_TMDR(base)     ((base) + 0x02)
#define REG_MTU4_TMDR(base)     ((base) + 0x03)
#define REG_MTU0_TIORH(base)    ((base) + 0x102)
#define REG_MTU1_TIOR(base)     ((base) + 0x182) 
#define REG_MTU2_TIOR(base)     ((base) + 0x202)
#define REG_MTU3_TIORH(base)    ((base) + 0x04)
#define REG_MTU4_TIORH(base)    ((base) + 0x06)
#define REG_MTU0_TIORL(base)    ((base) + 0x103)
#define REG_MTU3_TIORL(base)    ((base) + 0x05)
#define REG_MTU4_TIORL(base)    ((base) + 0x07)
#define REG_MTU5_TIORU(base)    ((base) + 0x286)
#define REG_MTU5_TIORV(base)    ((base) + 0x296)
#define REG_MTU5_TIORW(base)    ((base) + 0x2a6)
#define REG_MTU_TCNTCMPCLR(base)	((base) + 0x2b6)
#define REG_MTU0_TIER(base)		((base) + 0x104)
#define REG_MTU1_TIER(base)		((base) + 0x184)
#define REG_MTU2_TIER(base)		((base) + 0x204)
#define REG_MTU3_TIER(base)		((base) + 0x08)
#define REG_MTU4_TIER(base)		((base) + 0x09)
#define REG_MTU0_TIER2(base)	((base) + 0x124)
#define REG_MTU5_TIER(base)		((base) + 0x2b2)
#define REG_MTU0_TSR(base)		((base) + 0x105)
#define REG_MTU1_TSR(base)		((base) + 0x185)
#define REG_MTU2_TSR(base)		((base) + 0x205)
#define	REG_MTU3_TSR(base)		((base) + 0x2c)
#define REG_MTU4_TSR(base)		((base) + 0x2d)
#define REG_MTU0_TBTM(base)		((base) + 0x126)
#define REG_MTU3_TBTM(base)		((base) + 0x38)
#define REG_MTU4_TBTM(base)		((base) + 0x39)
#define REG_MTU1_TICCR(base)		((base) + 0x190)
#define REG_MTU4_TADCR(base)		((base) + 0x40)
#define REG_MTU4_TADCORA(base)		((base) + 0x44)
#define REG_MTU4_TADCORB(base)		((base) + 0x46)
#define REG_MTU4_TADCOBRA(base)		((base) + 0x48)
#define REG_MTU4_TADCOBRB(base)		((base) + 0x4a)
#define REG_MTU0_TCNT(base)		((base) + 0x106)
#define REG_MTU1_TCNT(base)		((base) + 0x186)
#define REG_MTU2_TCNT(base)		((base) + 0x206)
#define	REG_MTU3_TCNT(base)		((base) + 0x10)
#define REG_MTU4_TCNT(base)		((base) + 0x12)
#define REG_MTU5_TCNTU(base)		((base) + 0x280)
#define REG_MTU5_TCNTV(base)		((base) + 0x290)
#define REG_MTU5_TCNTW(base)		((base) + 0x2a0)

#define REG_MTU0_TGRA(base)		((base) + 0x108)
#define REG_MTU0_TGRB(base)		((base) + 0x10a)
#define REG_MTU0_TGRC(base)		((base) + 0x10c)
#define REG_MTU0_TGRD(base)		((base) + 0x10e)
#define REG_MTU0_TGRE(base)		((base) + 0x120)
#define REG_MTU0_TGRF(base)		((base) + 0x122)
#define REG_MTU1_TGRA(base)		((base) + 0x188)
#define REG_MTU1_TGRB(base)		((base) + 0x18a)
#define REG_MTU2_TGRA(base)		((base) + 0x208)
#define REG_MTU2_TGRB(base)		((base) + 0x20a)
#define REG_MTU3_TGRA(base)		((base) + 0x18)
#define REG_MTU3_TGRB(base)		((base) + 0x1a)
#define REG_MTU3_TGRC(base)		((base) + 0x24)
#define REG_MTU3_TGRD(base)		((base) + 0x26)
#define REG_MTU4_TGRA(base)		((base) + 0x1c)
#define REG_MTU4_TGRB(base)		((base) + 0x1e)
#define REG_MTU4_TGRC(base)		((base) + 0x28)
#define REG_MTU4_TGRD(base)		((base) + 0x2a)
#define REG_MTU5_TGRU5(base)		((base) + 0x282)
#define	REG_MTU5_TGRV5(base)		((base) + 0x292)
#define REG_MTU5_TGRW5(base)		((base) + 0x2a2)
#define REG_MTU_TSTR(base)		((base) + 0x80)
#define REG_MTU5_TSTR(base)		((base) + 0x2b4)
#define REG_MTU_TSYSR(base)		((base) + 0x81)
#define REG_MTU_TRWER(base)		((base) + 0x84)
#define REG_MTU_TOER(base)		((base) + 0x0a)
#define REG_MTU_TOCR1(base)		((base) + 0x0e)
#define REG_MTU_TOCR2(base)		((base) + 0x0f)
#define REG_MTU_TOLBR(base)		((base) + 0x36)
#define REG_MTU_TGCR(base)		((base) + 0x0d)
#define	REG_MTU_TCNTS(base)		((base) + 0x20)
#define REG_MTU_TDDR(base)		((base) + 0x16)
#define REG_MTU_TCDR(base)		((base) + 0x14)
#define REG_MTU_TCBR(base)		((base) + 0x22)
#define REG_MTU_TITCR(base)		((base) + 0x30)
#define REG_MTU_TITCNT(base)		((base) + 0x31)
#define REG_MTU_TBTER(base)		((base) + 0x32)
#define REG_MTU_TDER(base)		((base) + 0x34)
#define REG_MTU_TWCR(base)		((base) + 0x60)
#define REG_MTU0_NFCR(base)		((base) + 0x90)
#define REG_MTU1_NFCR(base)		((base) + 0x91)
#define REG_MTU2_NFCR(base)		((base) + 0x92)
#define REG_MTU3_NFCR(base)		((base) + 0x93)
#define REG_MTU4_NFCR(base)		((base) + 0x94)
#define REG_MTU5_NFCR(base)		((base) + 0x95)

typedef struct MTU2a {
    BusDevice bdev;
    Clock_t *clkIn;
    Clock_t *clkMTU[5];
    Clock_t *clkMTU5[3];
    SigNode *sigIrqTGIA[5];
    SigNode *sigIrqTGIB[5];
    SigNode *sigIrqTGIC[5];
    SigNode *sigIrqTGID[5];
    SigNode *sigIrqTCIV[5];
    SigNode *sigIrqTCIU[5];
    SigNode *sigIrqTGIE0;
    SigNode *sigIrqTGIF0;
    SigNode *sigIrqTGIW5;
    SigNode *sigIrqTGIV5;
    SigNode *sigIrqTGIU5;
    uint8_t regTCR[5];
    uint8_t regTCRU5;
    uint8_t regTCRV5;
    uint8_t regTCRW5;

    uint8_t regTMDR[5];
    
    uint8_t regTIORH[5];
    uint8_t regTIORL[5]; /* Sparse, only 0, 3, 4 */
    uint8_t regTIORU5; 
    uint8_t regTIORV5; 
    uint8_t regTIORW5; 
    uint8_t regTCNTCMPCLR;
    
    /* Interrupt enable */
    uint8_t regTIER[6];
    uint8_t regTIER2; /* MTU 0 only */

    uint8_t regTSR[5];
    uint8_t regTBTM[5];     /* Only MTU0, 3, 4 */

    /* The counters */
    uint16_t regTCNT[5];
    uint16_t regTCNTU;
    uint16_t regTCNTV;
    uint16_t regTCNTW;

    uint8_t regTICCR;
    uint8_t regTADCR;
    uint8_t regTADCORA;
    uint8_t regTADCORB;
    uint8_t regTADCOBRA;
    uint8_t regTADCOBRB;

    uint16_t regTGRA[5];
    uint16_t regTGRB[5];
    uint16_t regTGRC[5];
    uint16_t regTGRD[5];
    uint16_t regTGRE[1];
    uint16_t regTGRF[1];
    uint16_t regTGRU5;
    uint16_t regTGRV5;
    uint16_t regTGRW5;

    uint8_t regTSTR;
    uint8_t regTSTR5;
    uint8_t regTSYSR;
    uint8_t regNFCR;
} MTU2a;

/*
 *
 */
static uint32_t
mtu0_tcnt_read(void *clientData, uint32_t address, int rqlen)
{
    MTU2a *mtu = clientData;
    return mtu->regTCNT[0];
}
static void
mtu0_tcnt_write(void *clientData, uint32_t value, uint32_t address, int rqlen)
{
    MTU2a *mtu = clientData;
    mtu->regTCNT[0] = value;
}
static uint32_t
mtu1_tcnt_read(void *clientData, uint32_t address, int rqlen)
{
    MTU2a *mtu = clientData;
    return mtu->regTCNT[1];
}
static void
mtu1_tcnt_write(void *clientData, uint32_t value, uint32_t address, int rqlen)
{
    MTU2a *mtu = clientData;
    mtu->regTCNT[1] = value;
}
static uint32_t
mtu2_tcnt_read(void *clientData, uint32_t address, int rqlen)
{
    MTU2a *mtu = clientData;
    return mtu->regTCNT[2];
}
static void
mtu2_tcnt_write(void *clientData, uint32_t value, uint32_t address, int rqlen)
{
    MTU2a *mtu = clientData;
    mtu->regTCNT[2] = value;
}
static uint32_t
mtu3_tcnt_read(void *clientData, uint32_t address, int rqlen)
{
    MTU2a *mtu = clientData;
    return mtu->regTCNT[3];
}
static void
mtu3_tcnt_write(void *clientData, uint32_t value, uint32_t address, int rqlen)
{
    MTU2a *mtu = clientData;
    mtu->regTCNT[3] = value;
}
static uint32_t
mtu4_tcnt_read(void *clientData, uint32_t address, int rqlen)
{
    MTU2a *mtu = clientData;
    return mtu->regTCNT[4];
}
static void
mtu4_tcnt_write(void *clientData, uint32_t value, uint32_t address, int rqlen)
{
    MTU2a *mtu = clientData;
    mtu->regTCNT[4] = value;
}

static uint32_t
mtu5_tcntu_read(void *clientData, uint32_t address, int rqlen)
{
    MTU2a *mtu = clientData;
    return mtu->regTCNTU;
}
static void
mtu5_tcntu_write(void *clientData, uint32_t value, uint32_t address, int rqlen)
{
    MTU2a *mtu = clientData;
    mtu->regTCNTU = value;
}

static uint32_t
mtu5_tcntv_read(void *clientData, uint32_t address, int rqlen)
{
    MTU2a *mtu = clientData;
    return mtu->regTCNTV;
}
static void
mtu5_tcntv_write(void *clientData, uint32_t value, uint32_t address, int rqlen)
{
    MTU2a *mtu = clientData;
    mtu->regTCNTV = value;
}

static uint32_t
mtu5_tcntw_read(void *clientData, uint32_t address, int rqlen)
{
    MTU2a *mtu = clientData;
    return mtu->regTCNTW;
}
static void
mtu5_tcntw_write(void *clientData, uint32_t value, uint32_t address, int rqlen)
{
    MTU2a *mtu = clientData;
    mtu->regTCNTW = value;
}

/**
 **************************************************************************************
 * TCR registers
 **************************************************************************************
 */
static uint32_t
mtu0_tcr_read(void *clientData, uint32_t address, int rqlen)
{
    MTU2a *mtu = clientData;
    return mtu->regTCR[0];
}

static void
mtu0_tcr_write(void *clientData, uint32_t value, uint32_t address, int rqlen)
{
    MTU2a *mtu = clientData;
    uint8_t tpsc = value & 7;
    unsigned int div = 0;
    Clock_t *clkSrc = NULL;
    switch (tpsc) {
        case 0:
            clkSrc = mtu->clkIn;
            div = 1;
            break;

        case 1:
            clkSrc = mtu->clkIn;
            div = 4;
            break;

        case 2:
            clkSrc = mtu->clkIn;
            div = 16;
            break;

        case 3:
            clkSrc = mtu->clkIn;
            div = 64;
            break;

        default:
            fprintf(stderr, "%s: tpsc %u not implemented\n", __func__, tpsc);
            break;
    }
    if (clkSrc && div) {
        Clock_MakeDerived(mtu->clkMTU[0], clkSrc, 1, div);
    } 
    mtu->regTCR[0] = value;
}

static uint32_t
mtu1_tcr_read(void *clientData, uint32_t address, int rqlen)
{
    MTU2a *mtu = clientData;
    return mtu->regTCR[1];
}

static void
mtu1_tcr_write(void *clientData, uint32_t value, uint32_t address, int rqlen)
{
    MTU2a *mtu = clientData;
    uint8_t tpsc = value & 7;
    unsigned int div = 0;
    Clock_t *clkSrc = NULL;
    switch (tpsc) {
        case 0:
            clkSrc = mtu->clkIn;
            div = 1;
            break;

        case 1:
            clkSrc = mtu->clkIn;
            div = 4;
            break;

        case 2:
            clkSrc = mtu->clkIn;
            div = 16;
            break;

        case 3:
            clkSrc = mtu->clkIn;
            div = 64;
            break;

        case 6:
            clkSrc = mtu->clkIn;
            div = 256;
            break;

        default:
            fprintf(stderr, "%s: tpsc %u not implemented\n", __func__, tpsc);
            break;
    }
    if (clkSrc && div) {
        Clock_MakeDerived(mtu->clkMTU[1], clkSrc, 1, div);
    } 
    mtu->regTCR[1] = value;
}

static uint32_t
mtu2_tcr_read(void *clientData, uint32_t address, int rqlen)
{
    MTU2a *mtu = clientData;
    return mtu->regTCR[2];
}

static void
mtu2_tcr_write(void *clientData, uint32_t value, uint32_t address, int rqlen)
{
    MTU2a *mtu = clientData;
    uint8_t tpsc = value & 7;
    unsigned int div = 0;
    Clock_t *clkSrc = NULL;
    switch (tpsc) {
        case 0:
            clkSrc = mtu->clkIn;
            div = 1;
            break;

        case 1:
            clkSrc = mtu->clkIn;
            div = 4;
            break;

        case 2:
            clkSrc = mtu->clkIn;
            div = 16;
            break;

        case 3:
            clkSrc = mtu->clkIn;
            div = 64;
            break;

        case 7:
            clkSrc = mtu->clkIn;
            div = 1024;
            break;

        default:
            fprintf(stderr, "%s: tpsc %u not implemented\n", __func__, tpsc);
            break;
    }
    if (clkSrc && div) {
        Clock_MakeDerived(mtu->clkMTU[2], clkSrc, 1, div);
    } 
    mtu->regTCR[2] = value;
}

static uint32_t
mtu3_tcr_read(void *clientData, uint32_t address, int rqlen)
{
    MTU2a *mtu = clientData;
    return mtu->regTCR[3];
}

static void
mtu3_tcr_write(void *clientData, uint32_t value, uint32_t address, int rqlen)
{
    MTU2a *mtu = clientData;
    uint8_t tpsc = value & 7;
    unsigned int div = 0;
    Clock_t *clkSrc = NULL;
    switch (tpsc) {
        case 0:
            clkSrc = mtu->clkIn;
            div = 1;
            break;

        case 1:
            clkSrc = mtu->clkIn;
            div = 4;
            break;

        case 2:
            clkSrc = mtu->clkIn;
            div = 16;
            break;

        case 3:
            clkSrc = mtu->clkIn;
            div = 64;
            break;

        case 4:
            clkSrc = mtu->clkIn;
            div = 256;
            break;

        case 5:
            clkSrc = mtu->clkIn;
            div = 1024;
            break;

        default:
            fprintf(stderr, "%s: tpsc %u not implemented\n", __func__, tpsc);
            break;
    }
    if (clkSrc && div) {
        Clock_MakeDerived(mtu->clkMTU[3], clkSrc, 1, div);
    } 
    mtu->regTCR[3] = value;
}

static uint32_t
mtu4_tcr_read(void *clientData, uint32_t address, int rqlen)
{
    MTU2a *mtu = clientData;
    return mtu->regTCR[4];
}

static void
mtu4_tcr_write(void *clientData, uint32_t value, uint32_t address, int rqlen)
{
    MTU2a *mtu = clientData;
    uint8_t tpsc = value & 7;
    unsigned int div = 0;
    Clock_t *clkSrc = NULL;
    switch (tpsc) {
        case 0:
            clkSrc = mtu->clkIn;
            div = 1;
            break;

        case 1:
            clkSrc = mtu->clkIn;
            div = 4;
            break;

        case 2:
            clkSrc = mtu->clkIn;
            div = 16;
            break;

        case 3:
            clkSrc = mtu->clkIn;
            div = 64;
            break;

        case 4:
            clkSrc = mtu->clkIn;
            div = 256;
            break;

        case 5:
            clkSrc = mtu->clkIn;
            div = 1024;
            break;

        default:
            fprintf(stderr, "%s: tpsc %u not implemented\n", __func__, tpsc);
            break;
    }
    if (clkSrc && div) {
        Clock_MakeDerived(mtu->clkMTU[4], clkSrc, 1, div);
    } 
    mtu->regTCR[4] = value;
}

static uint32_t
mtu5_tcru_read(void *clientData, uint32_t address, int rqlen)
{
    MTU2a *mtu = clientData;
    return mtu->regTCRU5;
}

static void
mtu5_tcru_write(void *clientData, uint32_t value, uint32_t address, int rqlen)
{
    MTU2a *mtu = clientData;
    uint8_t tpsc = value & 7;
    unsigned int div = 0;
    Clock_t *clkSrc = NULL;
    switch (tpsc) {
        case 0:
            clkSrc = mtu->clkIn;
            div = 1;
            break;

        case 1:
            clkSrc = mtu->clkIn;
            div = 4;
            break;

        case 2:
            clkSrc = mtu->clkIn;
            div = 16;
            break;

        case 3:
            clkSrc = mtu->clkIn;
            div = 64;
            break;

        default:
            fprintf(stderr, "%s: tpsc %u is illegal\n", __func__, tpsc);
            break;
    }
    if (clkSrc && div) {
        Clock_MakeDerived(mtu->clkMTU5[0], clkSrc, 1, div);
    } 
    mtu->regTCRU5 = value;
}

static uint32_t
mtu5_tcrv_read(void *clientData, uint32_t address, int rqlen)
{
    MTU2a *mtu = clientData;
    return mtu->regTCRV5;
}

static void
mtu5_tcrv_write(void *clientData, uint32_t value, uint32_t address, int rqlen)
{
    MTU2a *mtu = clientData;
    uint8_t tpsc = value & 7;
    unsigned int div = 0;
    Clock_t *clkSrc = NULL;
    switch (tpsc) {
        case 0:
            clkSrc = mtu->clkIn;
            div = 1;
            break;

        case 1:
            clkSrc = mtu->clkIn;
            div = 4;
            break;

        case 2:
            clkSrc = mtu->clkIn;
            div = 16;
            break;

        case 3:
            clkSrc = mtu->clkIn;
            div = 64;
            break;

        default:
            fprintf(stderr, "%s: tpsc %u is illegal\n", __func__, tpsc);
            break;
    }
    if (clkSrc && div) {
        Clock_MakeDerived(mtu->clkMTU5[1], clkSrc, 1, div);
    } 
    mtu->regTCRV5 = value;
}

static uint32_t
mtu5_tcrw_read(void *clientData, uint32_t address, int rqlen)
{
    MTU2a *mtu = clientData;
    return mtu->regTCRW5;
}

static void
mtu5_tcrw_write(void *clientData, uint32_t value, uint32_t address, int rqlen)
{
    MTU2a *mtu = clientData;
    uint8_t tpsc = value & 7;
    unsigned int div = 0;
    Clock_t *clkSrc = NULL;
    switch (tpsc) {
        case 0:
            clkSrc = mtu->clkIn;
            div = 1;
            break;

        case 1:
            clkSrc = mtu->clkIn;
            div = 4;
            break;

        case 2:
            clkSrc = mtu->clkIn;
            div = 16;
            break;

        case 3:
            clkSrc = mtu->clkIn;
            div = 64;
            break;

        default:
            fprintf(stderr, "%s: tpsc %u is illegal\n", __func__, tpsc);
            break;
    }
    if (clkSrc && div) {
        Clock_MakeDerived(mtu->clkMTU5[2], clkSrc, 1, div);
    } 
    mtu->regTCRW5 = value;
}

static uint32_t
mtu0_tmdr_read(void *clientData, uint32_t address, int rqlen)
{
    MTU2a *mtu = clientData;
    return mtu->regTMDR[0];
}

static void
mtu0_tmdr_write(void *clientData, uint32_t value, uint32_t address, int rqlen)
{
    MTU2a *mtu = clientData;
    mtu->regTMDR[0] = value;
}

static uint32_t
mtu1_tmdr_read(void *clientData, uint32_t address, int rqlen)
{
    MTU2a *mtu = clientData;
    return mtu->regTMDR[1];
}

static void
mtu1_tmdr_write(void *clientData, uint32_t value, uint32_t address, int rqlen)
{
    MTU2a *mtu = clientData;
    mtu->regTMDR[1] = value;
}

static uint32_t
mtu2_tmdr_read(void *clientData, uint32_t address, int rqlen)
{
    MTU2a *mtu = clientData;
    return mtu->regTMDR[2];
}

static void
mtu2_tmdr_write(void *clientData, uint32_t value, uint32_t address, int rqlen)
{
    MTU2a *mtu = clientData;
    mtu->regTMDR[2] = value;
}

static uint32_t
mtu3_tmdr_read(void *clientData, uint32_t address, int rqlen)
{
    MTU2a *mtu = clientData;
    return mtu->regTMDR[3];
}

static void
mtu3_tmdr_write(void *clientData, uint32_t value, uint32_t address, int rqlen)
{
    MTU2a *mtu = clientData;
    mtu->regTMDR[3] = value;
}

static uint32_t
mtu4_tmdr_read(void *clientData, uint32_t address, int rqlen)
{
    MTU2a *mtu = clientData;
    return mtu->regTMDR[4];
}

static void
mtu4_tmdr_write(void *clientData, uint32_t value, uint32_t address, int rqlen)
{
    MTU2a *mtu = clientData;
    mtu->regTMDR[4] = value;
}

static uint32_t
mtu0_tiorh_read(void *clientData, uint32_t address, int rqlen)
{
    MTU2a *mtu = clientData;
    return mtu->regTIORH[0];
}

static void
mtu0_tiorh_write(void *clientData, uint32_t value, uint32_t address, int rqlen)
{
    MTU2a *mtu = clientData;
    mtu->regTIORH[0] = value;
    // update pin state (if cst = 0) and event sequencer
}

static uint32_t
mtu1_tiorh_read(void *clientData, uint32_t address, int rqlen)
{
    MTU2a *mtu = clientData;
    return mtu->regTIORH[1];
}

static void
mtu1_tiorh_write(void *clientData, uint32_t value, uint32_t address, int rqlen)
{
    MTU2a *mtu = clientData;
    mtu->regTIORH[1] = value;
    // update pin state (if cst = 0) and event sequencer
}

static uint32_t
mtu2_tiorh_read(void *clientData, uint32_t address, int rqlen)
{
    MTU2a *mtu = clientData;
    return mtu->regTIORH[2];
}

static void
mtu2_tiorh_write(void *clientData, uint32_t value, uint32_t address, int rqlen)
{
    MTU2a *mtu = clientData;
    mtu->regTIORH[2] = value;
    // update pin state (if cst = 0) and event sequencer
}

static uint32_t
mtu3_tiorh_read(void *clientData, uint32_t address, int rqlen)
{
    MTU2a *mtu = clientData;
    return mtu->regTIORH[3];
}

static void
mtu3_tiorh_write(void *clientData, uint32_t value, uint32_t address, int rqlen)
{
    MTU2a *mtu = clientData;
    mtu->regTIORH[3] = value;
    // update pin state (if cst = 0) and event sequencer
}

static uint32_t
mtu4_tiorh_read(void *clientData, uint32_t address, int rqlen)
{
    MTU2a *mtu = clientData;
    return mtu->regTIORH[4];
}

static void
mtu4_tiorh_write(void *clientData, uint32_t value, uint32_t address, int rqlen)
{
    MTU2a *mtu = clientData;
    mtu->regTIORH[4] = value;
    // update pin state (if cst = 0) and event sequencer
}

static uint32_t
mtu0_tiorl_read(void *clientData, uint32_t address, int rqlen)
{
    MTU2a *mtu = clientData;
    return mtu->regTIORL[0];
}

static void
mtu0_tiorl_write(void *clientData, uint32_t value, uint32_t address, int rqlen)
{
    MTU2a *mtu = clientData;
    mtu->regTIORL[0] = value;
    // update pin state (if cst = 0) and event sequencer
}
static uint32_t
mtu3_tiorl_read(void *clientData, uint32_t address, int rqlen)
{
    MTU2a *mtu = clientData;
    return mtu->regTIORL[3];
}

static void
mtu3_tiorl_write(void *clientData, uint32_t value, uint32_t address, int rqlen)
{
    MTU2a *mtu = clientData;
    mtu->regTIORL[3] = value;
    // update pin state (if cst = 0) and event sequencer
}

static uint32_t
mtu4_tiorl_read(void *clientData, uint32_t address, int rqlen)
{
    MTU2a *mtu = clientData;
    return mtu->regTIORL[4];
}

static void
mtu4_tiorl_write(void *clientData, uint32_t value, uint32_t address, int rqlen)
{
    MTU2a *mtu = clientData;
    mtu->regTIORL[4] = value;
    // update pin state (if cst = 0) and event sequencer
}

static uint32_t
mtu5_tioru5_read(void *clientData, uint32_t address, int rqlen)
{
    MTU2a *mtu = clientData;
    return mtu->regTIORU5;
}

static void
mtu5_tioru5_write(void *clientData, uint32_t value, uint32_t address, int rqlen)
{
    MTU2a *mtu = clientData;
    mtu->regTIORU5 = value;
    // update pin state (if cst = 0) and event sequencer
}

static uint32_t
mtu5_tiorv5_read(void *clientData, uint32_t address, int rqlen)
{
    MTU2a *mtu = clientData;
    return mtu->regTIORV5;
}

static void
mtu5_tiorv5_write(void *clientData, uint32_t value, uint32_t address, int rqlen)
{
    MTU2a *mtu = clientData;
    mtu->regTIORV5 = value;
    // update pin state (if cst = 0) and event sequencer
}

static uint32_t
mtu5_tiorw5_read(void *clientData, uint32_t address, int rqlen)
{
    MTU2a *mtu = clientData;
    return mtu->regTIORW5;
}

static void
mtu5_tiorw5_write(void *clientData, uint32_t value, uint32_t address, int rqlen)
{
    MTU2a *mtu = clientData;
    mtu->regTIORW5 = value;
    // update pin state (if cst = 0) and event sequencer
}

static uint32_t
mtu_tcntcmpclr_read(void *clientData, uint32_t address, int rqlen)
{
    MTU2a *mtu = clientData;
    return mtu->regTCNTCMPCLR;
}

static void
mtu_tcntcmpclr_write(void *clientData, uint32_t value, uint32_t address, int rqlen)
{
    MTU2a *mtu = clientData;
    mtu->regTCNTCMPCLR = value;
}

/* Interrupt enable */
static uint32_t
mtu0_tier_read(void *clientData, uint32_t address, int rqlen)
{
    MTU2a *mtu = clientData;
    return mtu->regTIER[0];
}

static void
mtu0_tier_write(void *clientData, uint32_t value, uint32_t address, int rqlen)
{
    MTU2a *mtu = clientData;
    mtu->regTIER[0] = value;
}
static uint32_t
mtu0_tier2_read(void *clientData, uint32_t address, int rqlen)
{
    MTU2a *mtu = clientData;
    return mtu->regTIER2;
}

static void
mtu0_tier2_write(void *clientData, uint32_t value, uint32_t address, int rqlen)
{
    MTU2a *mtu = clientData;
    mtu->regTIER2 = value;
}

static uint32_t
mtu1_tier_read(void *clientData, uint32_t address, int rqlen)
{
    MTU2a *mtu = clientData;
    return mtu->regTIER[1];
}

static void
mtu1_tier_write(void *clientData, uint32_t value, uint32_t address, int rqlen)
{
    MTU2a *mtu = clientData;
    mtu->regTIER[1] = value;
}

static uint32_t
mtu2_tier_read(void *clientData, uint32_t address, int rqlen)
{
    MTU2a *mtu = clientData;
    return mtu->regTIER[2];
}

static void
mtu2_tier_write(void *clientData, uint32_t value, uint32_t address, int rqlen)
{
    MTU2a *mtu = clientData;
    mtu->regTIER[2] = value;
}
static uint32_t
mtu3_tier_read(void *clientData, uint32_t address, int rqlen)
{
    MTU2a *mtu = clientData;
    return mtu->regTIER[3];
}

static void
mtu3_tier_write(void *clientData, uint32_t value, uint32_t address, int rqlen)
{
    MTU2a *mtu = clientData;
    mtu->regTIER[3] = value;
}

static uint32_t
mtu4_tier_read(void *clientData, uint32_t address, int rqlen)
{
    MTU2a *mtu = clientData;
    return mtu->regTIER[4];
}

static void
mtu4_tier_write(void *clientData, uint32_t value, uint32_t address, int rqlen)
{
    MTU2a *mtu = clientData;
    mtu->regTIER[4] = value;
}

static uint32_t
mtu5_tier_read(void *clientData, uint32_t address, int rqlen)
{
    MTU2a *mtu = clientData;
    return mtu->regTIER[5];
}

static void
mtu5_tier_write(void *clientData, uint32_t value, uint32_t address, int rqlen)
{
    MTU2a *mtu = clientData;
    mtu->regTIER[5] = value;
}

static uint32_t
mtu0_tsr_read(void *clientData, uint32_t address, int rqlen)
{
    MTU2a *mtu = clientData;
    return mtu->regTSR[0];
}

static void
mtu0_tsr_write(void *clientData, uint32_t value, uint32_t address, int rqlen)
{
    MTU2a *mtu = clientData;
    mtu->regTSR[0] = value;
}
static uint32_t
mtu1_tsr_read(void *clientData, uint32_t address, int rqlen)
{
    MTU2a *mtu = clientData;
    return mtu->regTSR[1];
}

static void
mtu1_tsr_write(void *clientData, uint32_t value, uint32_t address, int rqlen)
{
    MTU2a *mtu = clientData;
    mtu->regTSR[1] = value;
}
static uint32_t
mtu2_tsr_read(void *clientData, uint32_t address, int rqlen)
{
    MTU2a *mtu = clientData;
    return mtu->regTSR[2];
}

static void
mtu2_tsr_write(void *clientData, uint32_t value, uint32_t address, int rqlen)
{
    MTU2a *mtu = clientData;
    mtu->regTSR[2] = value;
}
static uint32_t
mtu3_tsr_read(void *clientData, uint32_t address, int rqlen)
{
    MTU2a *mtu = clientData;
    return mtu->regTSR[3];
}

static void
mtu3_tsr_write(void *clientData, uint32_t value, uint32_t address, int rqlen)
{
    MTU2a *mtu = clientData;
    mtu->regTSR[3] = value;
}
static uint32_t
mtu4_tsr_read(void *clientData, uint32_t address, int rqlen)
{
    MTU2a *mtu = clientData;
    return mtu->regTSR[4];
}

static void
mtu4_tsr_write(void *clientData, uint32_t value, uint32_t address, int rqlen)
{
    MTU2a *mtu = clientData;
    mtu->regTSR[4] = value;
}

static uint32_t
mtu0_tbtm_read(void *clientData, uint32_t address, int rqlen)
{
    MTU2a *mtu = clientData;
    return mtu->regTBTM[0];
}

static void
mtu0_tbtm_write(void *clientData, uint32_t value, uint32_t address, int rqlen)
{

    MTU2a *mtu = clientData;
    mtu->regTBTM[0] = value;
    // update timeout
}

static uint32_t
mtu3_tbtm_read(void *clientData, uint32_t address, int rqlen)
{
    MTU2a *mtu = clientData;
    return mtu->regTBTM[3];
}
static void
mtu3_tbtm_write(void *clientData, uint32_t value, uint32_t address, int rqlen)
{

    MTU2a *mtu = clientData;
    mtu->regTBTM[3] = value;
    // update timeout
}

static uint32_t
mtu4_tbtm_read(void *clientData, uint32_t address, int rqlen)
{
    MTU2a *mtu = clientData;
    return mtu->regTGRA[4];
}
static void
mtu4_tbtm_write(void *clientData, uint32_t value, uint32_t address, int rqlen)
{

    MTU2a *mtu = clientData;
    mtu->regTBTM[4] = value;
    // update timeout
}

static uint32_t
mtu1_ticcr_read(void *clientData, uint32_t address, int rqlen)
{
    MTU2a *mtu = clientData;
    return mtu->regTICCR;
}

static void 
mtu1_ticcr_write(void *clientData, uint32_t value, uint32_t address, int rqlen)
{
    MTU2a *mtu = clientData;
    mtu->regTICCR = value;
}
static uint32_t
mtu4_tadcr_read(void *clientData, uint32_t address, int rqlen)
{
    MTU2a *mtu = clientData;
    return mtu->regTADCR;
}

static void
mtu4_tadcr_write(void *clientData, uint32_t value, uint32_t address, int rqlen)
{
    MTU2a *mtu = clientData;
    mtu->regTADCR = value;
}

static uint32_t
mtu4_tadcora_read(void *clientData, uint32_t address, int rqlen)
{
    MTU2a *mtu = clientData;
    return mtu->regTADCORA;
}

static void 
mtu4_tadcora_write(void *clientData, uint32_t value, uint32_t address, int rqlen)
{
    MTU2a *mtu = clientData;
    mtu->regTADCORA = value;
}

static uint32_t
mtu4_tadcorb_read(void *clientData, uint32_t address, int rqlen)
{
    MTU2a *mtu = clientData;
    return mtu->regTADCORB;
}

static void
mtu4_tadcorb_write(void *clientData, uint32_t value, uint32_t address, int rqlen)
{
    MTU2a *mtu = clientData;
    mtu->regTADCORB = value;
}
static uint32_t
mtu4_tadcobra_read(void *clientData, uint32_t address, int rqlen)
{
    MTU2a *mtu = clientData;
    return mtu->regTADCOBRA;
}

static void 
mtu4_tadcobra_write(void *clientData, uint32_t value, uint32_t address, int rqlen)
{
    MTU2a *mtu = clientData;
    mtu->regTADCOBRA = value;
}
static uint32_t
mtu4_tadcobrb_read(void *clientData, uint32_t address, int rqlen)
{
    MTU2a *mtu = clientData;
    return mtu->regTADCOBRB;
}

static void 
mtu4_tadcobrb_write(void *clientData, uint32_t value, uint32_t address, int rqlen)
{
    MTU2a *mtu = clientData;
    mtu->regTADCOBRB = value;
}

static uint32_t
mtu0_tgra_read(void *clientData, uint32_t address, int rqlen)
{
    MTU2a *mtu = clientData;
    return mtu->regTGRA[0];
}

static void
mtu0_tgra_write(void *clientData, uint32_t value, uint32_t address, int rqlen)
{

    MTU2a *mtu = clientData;
    mtu->regTGRA[0] = value;
    // update timeout
}
static uint32_t
mtu1_tgra_read(void *clientData, uint32_t address, int rqlen)
{
    MTU2a *mtu = clientData;
    return mtu->regTGRA[1];
}

static void
mtu1_tgra_write(void *clientData, uint32_t value, uint32_t address, int rqlen)
{

    MTU2a *mtu = clientData;
    mtu->regTGRA[1] = value;
    // update timeout
}
static uint32_t
mtu2_tgra_read(void *clientData, uint32_t address, int rqlen)
{
    MTU2a *mtu = clientData;
    return mtu->regTGRA[2];
}

static void
mtu2_tgra_write(void *clientData, uint32_t value, uint32_t address, int rqlen)
{

    MTU2a *mtu = clientData;
    mtu->regTGRA[2] = value;
    // update timeout
}

static uint32_t
mtu3_tgra_read(void *clientData, uint32_t address, int rqlen)
{
    MTU2a *mtu = clientData;
    return mtu->regTGRA[3];
}

static void
mtu3_tgra_write(void *clientData, uint32_t value, uint32_t address, int rqlen)
{

    MTU2a *mtu = clientData;
    mtu->regTGRA[3] = value;
    // update timeout
}

static uint32_t
mtu4_tgra_read(void *clientData, uint32_t address, int rqlen)
{
    MTU2a *mtu = clientData;
    return mtu->regTGRA[4];
}

static void
mtu4_tgra_write(void *clientData, uint32_t value, uint32_t address, int rqlen)
{

    MTU2a *mtu = clientData;
    mtu->regTGRA[4] = value;
    // update timeout
}

static uint32_t
mtu0_tgrb_read(void *clientData, uint32_t address, int rqlen)
{
    MTU2a *mtu = clientData;
    return mtu->regTGRB[0];
}

static void
mtu0_tgrb_write(void *clientData, uint32_t value, uint32_t address, int rqlen)
{

    MTU2a *mtu = clientData;
    mtu->regTGRB[0] = value;
    // update timeout
}
static uint32_t
mtu1_tgrb_read(void *clientData, uint32_t address, int rqlen)
{
    MTU2a *mtu = clientData;
    return mtu->regTGRB[1];
}

static void
mtu1_tgrb_write(void *clientData, uint32_t value, uint32_t address, int rqlen)
{

    MTU2a *mtu = clientData;
    mtu->regTGRB[1] = value;
    // update timeout
}
static uint32_t
mtu2_tgrb_read(void *clientData, uint32_t address, int rqlen)
{
    MTU2a *mtu = clientData;
    return mtu->regTGRB[2];
}

static void
mtu2_tgrb_write(void *clientData, uint32_t value, uint32_t address, int rqlen)
{

    MTU2a *mtu = clientData;
    mtu->regTGRB[2] = value;
    // update timeout
}

static uint32_t
mtu3_tgrb_read(void *clientData, uint32_t address, int rqlen)
{
    MTU2a *mtu = clientData;
    return mtu->regTGRB[3];
}

static void
mtu3_tgrb_write(void *clientData, uint32_t value, uint32_t address, int rqlen)
{

    MTU2a *mtu = clientData;
    mtu->regTGRB[3] = value;
    // update timeout
}

static uint32_t
mtu4_tgrb_read(void *clientData, uint32_t address, int rqlen)
{
    MTU2a *mtu = clientData;
    return mtu->regTGRA[4];
}

static void
mtu4_tgrb_write(void *clientData, uint32_t value, uint32_t address, int rqlen)
{

    MTU2a *mtu = clientData;
    mtu->regTGRB[4] = value;
    // update timeout
}

static uint32_t
mtu0_tgrc_read(void *clientData, uint32_t address, int rqlen)
{
    MTU2a *mtu = clientData;
    return mtu->regTGRC[0];
}

static void
mtu0_tgrc_write(void *clientData, uint32_t value, uint32_t address, int rqlen)
{

    MTU2a *mtu = clientData;
    mtu->regTGRC[0] = value;
    // update timeout
}


static uint32_t
mtu3_tgrc_read(void *clientData, uint32_t address, int rqlen)
{
    MTU2a *mtu = clientData;
    return mtu->regTGRC[3];
}

static void
mtu3_tgrc_write(void *clientData, uint32_t value, uint32_t address, int rqlen)
{

    MTU2a *mtu = clientData;
    mtu->regTGRC[3] = value;
    // update timeout
}
static uint32_t
mtu4_tgrc_read(void *clientData, uint32_t address, int rqlen)
{
    MTU2a *mtu = clientData;
    return mtu->regTGRC[4];
}

static void
mtu4_tgrc_write(void *clientData, uint32_t value, uint32_t address, int rqlen)
{

    MTU2a *mtu = clientData;
    mtu->regTGRC[4] = value;
    // update timeout
}

static uint32_t
mtu0_tgrd_read(void *clientData, uint32_t address, int rqlen)
{
    MTU2a *mtu = clientData;
    return mtu->regTGRD[0];
}

static void
mtu0_tgrd_write(void *clientData, uint32_t value, uint32_t address, int rqlen)
{

    MTU2a *mtu = clientData;
    mtu->regTGRD[0] = value;
    // update timeout
}

static uint32_t
mtu3_tgrd_read(void *clientData, uint32_t address, int rqlen)
{
    MTU2a *mtu = clientData;
    return mtu->regTGRD[3];
}

static void
mtu3_tgrd_write(void *clientData, uint32_t value, uint32_t address, int rqlen)
{

    MTU2a *mtu = clientData;
    mtu->regTGRD[3] = value;
    // update timeout
}

static uint32_t
mtu4_tgrd_read(void *clientData, uint32_t address, int rqlen)
{
    MTU2a *mtu = clientData;
    return mtu->regTGRD[4];
}

static void
mtu4_tgrd_write(void *clientData, uint32_t value, uint32_t address, int rqlen)
{

    MTU2a *mtu = clientData;
    mtu->regTGRD[4] = value;
    // update timeout
}
static uint32_t
mtu0_tgre_read(void *clientData, uint32_t address, int rqlen)
{
    MTU2a *mtu = clientData;
    return mtu->regTGRE[0];
}

static void
mtu0_tgre_write(void *clientData, uint32_t value, uint32_t address, int rqlen)
{

    MTU2a *mtu = clientData;
    mtu->regTGRE[0] = value;
    // update timeout
}

static uint32_t
mtu0_tgrf_read(void *clientData, uint32_t address, int rqlen)
{
    MTU2a *mtu = clientData;
    return mtu->regTGRF[0];
}

static void
mtu0_tgrf_write(void *clientData, uint32_t value, uint32_t address, int rqlen)
{

    MTU2a *mtu = clientData;
    mtu->regTGRF[0] = value;
    // update timeout
}

static uint32_t
mtu5_tgru5_read(void *clientData, uint32_t address, int rqlen)
{
    MTU2a *mtu = clientData;
    return mtu->regTGRU5;
}

static void
mtu5_tgru5_write(void *clientData, uint32_t value, uint32_t address, int rqlen)
{

    MTU2a *mtu = clientData;
    mtu->regTGRU5 = value;
    // update timeout
}
static uint32_t
mtu5_tgrv5_read(void *clientData, uint32_t address, int rqlen)
{
    MTU2a *mtu = clientData;
    return mtu->regTGRV5;
}

static void
mtu5_tgrv5_write(void *clientData, uint32_t value, uint32_t address, int rqlen)
{

    MTU2a *mtu = clientData;
    mtu->regTGRV5 = value;
    // update timeout
}
static uint32_t
mtu5_tgrw5_read(void *clientData, uint32_t address, int rqlen)
{
    MTU2a *mtu = clientData;
    return mtu->regTGRW5;
}

static void
mtu5_tgrw5_write(void *clientData, uint32_t value, uint32_t address, int rqlen)
{

    MTU2a *mtu = clientData;
    mtu->regTGRW5 = value;
    // update timeout
}


static uint32_t
mtu_tstr_read(void *clientData, uint32_t address, int rqlen)
{
    MTU2a *mtu = clientData;
    return mtu->regTSTR;
}

static void
mtu_tstr_write(void *clientData, uint32_t value, uint32_t address, int rqlen)
{
    MTU2a *mtu = clientData;
    mtu->regTSTR = value;
}

static uint32_t
mtu5_tstr_read(void *clientData, uint32_t address, int rqlen)
{
    MTU2a *mtu = clientData;
    return mtu->regTSTR5;
}

static void
mtu5_tstr_write(void *clientData, uint32_t value, uint32_t address, int rqlen)
{
    MTU2a *mtu = clientData;
    mtu->regTSTR5 = value;
}

static void
MTU2a_UnMap(void *module_owner, uint32_t base, uint32_t mapsize)
{
}

static void
MTU2a_Map(void *module_owner, uint32_t base, uint32_t _mapsize, uint32_t flags)
{
    MTU2a *mtu = module_owner; 
    IOH_New8(REG_MTU0_TCNT(base), mtu0_tcnt_read, mtu0_tcnt_write, mtu);
    IOH_New8(REG_MTU1_TCNT(base), mtu1_tcnt_read, mtu1_tcnt_write, mtu);
    IOH_New8(REG_MTU2_TCNT(base), mtu2_tcnt_read, mtu2_tcnt_write, mtu);
    IOH_New8(REG_MTU3_TCNT(base), mtu3_tcnt_read, mtu3_tcnt_write, mtu);
    IOH_New8(REG_MTU4_TCNT(base), mtu4_tcnt_read, mtu4_tcnt_write, mtu);
    IOH_New8(REG_MTU5_TCNTU(base), mtu5_tcntu_read, mtu5_tcntu_write, mtu);
    IOH_New8(REG_MTU5_TCNTV(base), mtu5_tcntv_read, mtu5_tcntv_write, mtu);
    IOH_New8(REG_MTU5_TCNTW(base), mtu5_tcntw_read, mtu5_tcntw_write, mtu);

    IOH_New8(REG_MTU0_TCR(base), mtu0_tcr_read, mtu0_tcr_write, mtu);
    IOH_New8(REG_MTU1_TCR(base), mtu1_tcr_read, mtu1_tcr_write, mtu);
    IOH_New8(REG_MTU2_TCR(base), mtu2_tcr_read, mtu2_tcr_write, mtu);
    IOH_New8(REG_MTU3_TCR(base), mtu3_tcr_read, mtu3_tcr_write, mtu);
    IOH_New8(REG_MTU4_TCR(base), mtu4_tcr_read, mtu4_tcr_write, mtu);
    IOH_New8(REG_MTU5_TCRU(base), mtu5_tcru_read, mtu5_tcru_write, mtu);
    IOH_New8(REG_MTU5_TCRV(base), mtu5_tcrv_read, mtu5_tcrv_write, mtu);
    IOH_New8(REG_MTU5_TCRW(base), mtu5_tcrw_read, mtu5_tcrw_write, mtu);
    IOH_New8(REG_MTU0_TIORH(base), mtu0_tiorh_read, mtu0_tiorh_write, mtu);
    IOH_New8(REG_MTU1_TIOR(base), mtu1_tiorh_read, mtu1_tiorh_write, mtu);
    IOH_New8(REG_MTU2_TIOR(base), mtu2_tiorh_read, mtu2_tiorh_write, mtu);
    IOH_New8(REG_MTU3_TIORH(base), mtu3_tiorh_read, mtu3_tiorh_write, mtu);
    IOH_New8(REG_MTU4_TIORH(base), mtu4_tiorh_read, mtu4_tiorh_write, mtu);
    IOH_New8(REG_MTU0_TIORL(base), mtu0_tiorl_read, mtu0_tiorl_write, mtu);
    IOH_New8(REG_MTU3_TIORL(base), mtu3_tiorl_read, mtu3_tiorl_write, mtu);
    IOH_New8(REG_MTU4_TIORL(base), mtu4_tiorl_read, mtu4_tiorl_write, mtu);
    IOH_New8(REG_MTU5_TIORU(base), mtu5_tioru5_read, mtu5_tioru5_write, mtu);
    IOH_New8(REG_MTU5_TIORV(base), mtu5_tiorv5_read, mtu5_tiorv5_write, mtu);
    IOH_New8(REG_MTU5_TIORW(base), mtu5_tiorw5_read, mtu5_tiorw5_write, mtu);
    IOH_New8(REG_MTU_TCNTCMPCLR(base), mtu_tcntcmpclr_read, mtu_tcntcmpclr_write, mtu);

    IOH_New8(REG_MTU0_TMDR(base), mtu0_tmdr_read, mtu0_tmdr_write, mtu);
    IOH_New8(REG_MTU1_TMDR(base), mtu1_tmdr_read, mtu1_tmdr_write, mtu);
    IOH_New8(REG_MTU2_TMDR(base), mtu2_tmdr_read, mtu2_tmdr_write, mtu);
    IOH_New8(REG_MTU3_TMDR(base), mtu3_tmdr_read, mtu3_tmdr_write, mtu);
    IOH_New8(REG_MTU4_TMDR(base), mtu4_tmdr_read, mtu4_tmdr_write, mtu);

    IOH_New8(REG_MTU0_TIER(base), mtu0_tier_read, mtu0_tier_write, mtu);
    IOH_New8(REG_MTU1_TIER(base), mtu1_tier_read, mtu1_tier_write, mtu);
    IOH_New8(REG_MTU2_TIER(base), mtu2_tier_read, mtu2_tier_write, mtu);
    IOH_New8(REG_MTU3_TIER(base), mtu3_tier_read, mtu3_tier_write, mtu);
    IOH_New8(REG_MTU4_TIER(base), mtu4_tier_read, mtu4_tier_write, mtu);
    IOH_New8(REG_MTU5_TIER(base), mtu5_tier_read, mtu5_tier_write, mtu);
    IOH_New8(REG_MTU0_TIER2(base), mtu0_tier2_read, mtu0_tier2_write, mtu);

    IOH_New8(REG_MTU0_TSR(base), mtu0_tsr_read, mtu0_tsr_write, mtu);
    IOH_New8(REG_MTU1_TSR(base), mtu1_tsr_read, mtu1_tsr_write, mtu);
    IOH_New8(REG_MTU2_TSR(base), mtu2_tsr_read, mtu2_tsr_write, mtu);
    IOH_New8(REG_MTU3_TSR(base), mtu3_tsr_read, mtu3_tsr_write, mtu);
    IOH_New8(REG_MTU4_TSR(base), mtu4_tsr_read, mtu4_tsr_write, mtu);

    IOH_New8(REG_MTU0_TBTM(base), mtu0_tbtm_read, mtu0_tbtm_write, mtu);
    IOH_New8(REG_MTU3_TBTM(base), mtu3_tbtm_read, mtu3_tbtm_write, mtu);
    IOH_New8(REG_MTU4_TBTM(base), mtu4_tbtm_read, mtu4_tbtm_write, mtu);

    IOH_New8(REG_MTU1_TICCR(base), mtu1_ticcr_read, mtu1_ticcr_write, mtu);
    IOH_New8(REG_MTU4_TADCR(base), mtu4_tadcr_read, mtu4_tadcr_write, mtu);
    IOH_New8(REG_MTU4_TADCORA(base), mtu4_tadcora_read, mtu4_tadcora_write, mtu);
    IOH_New8(REG_MTU4_TADCORB(base), mtu4_tadcorb_read, mtu4_tadcorb_write, mtu);
    IOH_New8(REG_MTU4_TADCOBRA(base), mtu4_tadcobra_read, mtu4_tadcobra_write, mtu);
    IOH_New8(REG_MTU4_TADCOBRB(base), mtu4_tadcobrb_read, mtu4_tadcobrb_write, mtu);

    IOH_New8(REG_MTU0_TGRA(base), mtu0_tgra_read, mtu0_tgra_write, mtu);
    IOH_New8(REG_MTU1_TGRA(base), mtu1_tgra_read, mtu1_tgra_write, mtu);
    IOH_New8(REG_MTU2_TGRA(base), mtu2_tgra_read, mtu2_tgra_write, mtu);
    IOH_New8(REG_MTU3_TGRA(base), mtu3_tgra_read, mtu3_tgra_write, mtu);
    IOH_New8(REG_MTU4_TGRA(base), mtu4_tgra_read, mtu4_tgra_write, mtu);

    IOH_New8(REG_MTU0_TGRB(base), mtu0_tgrb_read, mtu0_tgrb_write, mtu);
    IOH_New8(REG_MTU1_TGRB(base), mtu1_tgrb_read, mtu1_tgrb_write, mtu);
    IOH_New8(REG_MTU2_TGRB(base), mtu2_tgrb_read, mtu2_tgrb_write, mtu);
    IOH_New8(REG_MTU3_TGRB(base), mtu3_tgrb_read, mtu3_tgrb_write, mtu);
    IOH_New8(REG_MTU4_TGRB(base), mtu4_tgrb_read, mtu4_tgrb_write, mtu);

    IOH_New8(REG_MTU0_TGRC(base), mtu0_tgrc_read, mtu0_tgrc_write, mtu);
    //IOH_New8(REG_MTU1_TGRC(base), mtu1_tgrc_read, mtu1_tgrc_write, mtu);
    //IOH_New8(REG_MTU2_TGRC(base), mtu2_tgrc_read, mtu2_tgrc_write, mtu);
    IOH_New8(REG_MTU3_TGRC(base), mtu3_tgrc_read, mtu3_tgrc_write, mtu);
    IOH_New8(REG_MTU4_TGRC(base), mtu4_tgrc_read, mtu4_tgrc_write, mtu);

    IOH_New8(REG_MTU0_TGRD(base), mtu0_tgrd_read, mtu0_tgrd_write, mtu);
    //IOH_New8(REG_MTU1_TGRD(base), mtu1_tgrd_read, mtu1_tgrd_write, mtu);
    //IOH_New8(REG_MTU2_TGRD(base), mtu2_tgrd_read, mtu2_tgrd_write, mtu);
    IOH_New8(REG_MTU3_TGRD(base), mtu3_tgrd_read, mtu3_tgrd_write, mtu);
    IOH_New8(REG_MTU4_TGRD(base), mtu4_tgrd_read, mtu4_tgrd_write, mtu);

    IOH_New8(REG_MTU_TSTR(base), mtu_tstr_read, mtu_tstr_write, mtu);
    IOH_New8(REG_MTU5_TSTR(base), mtu5_tstr_read, mtu5_tstr_write, mtu);

    IOH_New8(REG_MTU0_TGRE(base), mtu0_tgre_read, mtu0_tgre_write, mtu);
    IOH_New8(REG_MTU0_TGRF(base), mtu0_tgrf_read, mtu0_tgrf_write, mtu);
    IOH_New8(REG_MTU5_TGRU5(base), mtu5_tgru5_read, mtu5_tgru5_write, mtu);
    IOH_New8(REG_MTU5_TGRV5(base), mtu5_tgrv5_read, mtu5_tgrv5_write, mtu);
    IOH_New8(REG_MTU5_TGRW5(base), mtu5_tgrw5_read, mtu5_tgrw5_write, mtu);
}

BusDevice *
MTU2a_New(const char *name) {
    int i;
    MTU2a *mtu = sg_new(MTU2a);

    mtu->bdev.Map = MTU2a_Map;
    mtu->bdev.UnMap = MTU2a_UnMap;
    mtu->bdev.owner = mtu;
    mtu->bdev.hw_flags = MEM_FLAG_READABLE | MEM_FLAG_WRITABLE;

    mtu->clkIn = Clock_New("%s.clk", name);
    if (!mtu->clkIn) {  
        fprintf(stderr," Can not create clock signals for %s\n", name);
    }
    for ( i = 0; i < 5; i++) {
        mtu->clkMTU[i] = Clock_New("%s.clkMTU%u", name, i);
        if (!mtu->clkMTU[i]) {
            fprintf(stderr," Can not create clock signals for %s\n", name);
            exit(1);
        }
        Clock_MakeDerived(mtu->clkMTU[i], mtu->clkIn, 1, 1);
    }
    for ( i = 0; i < 3; i++) {
        mtu->clkMTU5[i] = Clock_New("%s.clkMTU5%c", name, i + 'U');
        if (!mtu->clkMTU5[i]) {
            fprintf(stderr," Can not create clock signals for %s\n", name);
            exit(1);
        }
        Clock_MakeDerived(mtu->clkMTU5[i], mtu->clkIn, 1, 1);
    }
    for ( i = 0; i < 5; i++) {
        mtu->sigIrqTGIA[i] = SigNode_New("%s.irqTGIA%u", name, i);
        mtu->sigIrqTGIB[i] = SigNode_New("%s.irqTGIB%u", name, i);
        mtu->sigIrqTGIC[i] = SigNode_New("%s.irqTGIC%u", name, i);
        mtu->sigIrqTGID[i] = SigNode_New("%s.irqTGID%u", name, i);
        mtu->sigIrqTCIV[i] = SigNode_New("%s.irqTCIV%u", name, i);
        mtu->sigIrqTCIU[i] = SigNode_New("%s.irqTCIU%u", name, i);

        if (!mtu->sigIrqTGIA[i] || !mtu->sigIrqTGIB[i] || !mtu->sigIrqTGIC[i] 
            || !mtu->sigIrqTGID[i] || !mtu->sigIrqTCIV[i] || !mtu->sigIrqTCIU[i]) 
        {
            fprintf(stderr," Can not create IRQ signals for %s\n", name);
            exit(1);
        }
    }
    mtu->sigIrqTGIE0 = SigNode_New("%s.irqTGIE0", name);
    mtu->sigIrqTGIF0 = SigNode_New("%s.irqTGIF0", name);
    mtu->sigIrqTGIW5 = SigNode_New("%s.irqTGIW5", name);
    mtu->sigIrqTGIV5 = SigNode_New("%s.irqTGIV5", name);
    mtu->sigIrqTGIU5 = SigNode_New("%s.irqTGIU5", name);

    if (!mtu->sigIrqTGIE0 || !mtu->sigIrqTGIF0 || !mtu->sigIrqTGIW5 
        || !mtu->sigIrqTGIV5 || !mtu->sigIrqTGIU5) 
    {
            fprintf(stderr," Can not create IRQ signals for %s\n", name);
            exit(1);
    }
    return &mtu->bdev;
}
