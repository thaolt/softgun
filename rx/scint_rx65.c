/**
 ***************************************************************************
 * Software configurable interrupt module for Renesas RX65
 * This is part of the ICU
 ***************************************************************************
 */

#include <stdint.h>
#include "bus.h"
#include "signode.h"
#include "sgstring.h"
#include "scint_rx65.h"

#define REG_PIBR(base, n)   ((base) + 0x700 + (n))
#define REG_PIAR(base, n)   ((base) + 0x900 + (n))
#define REG_SLIBXR(base, n) ((base) + 0x700 + (n))  /* n = 128 ... 143 */
#define REG_SLIBR(base, n)  ((base) + 0x700 + (n))  /* n = 144 ... 207 */
#define REG_SLIAR(base, n)  ((base) + 0x900 + (n))  /* n = 208 ... 255 */
#define REG_SLIPRCR(base)   ((base) + 0xa00) 
#define REG_SELEXDR(base)   ((base) + 0xa01) 

typedef struct ScintMod {
    BusDevice bdev;
    const char *name;
    SigNode *irqAIn[96];
    SigNode *irqBIn[96];
    SigNode *irqFwd[128];
    uint8_t regPIBR[12];
    uint8_t regPIAR[12];
    uint8_t regSLIR[128];
    uint8_t regSLIPRCR;
    uint8_t regSELEXDR;
} ScintMod;

static bool 
verify_writable(ScintMod *scint) {
    if (scint->regSLIPRCR & 1) {
        fprintf(stderr, "SC Interrupt is write protected\n");
        return false;
    } else {
        return true;
    }
}
static uint32_t
pibr_read(void *clientData, uint32_t address, int rqlen)
{
    return 0;
}

static void
pibr_write(void *clientData, uint32_t value, uint32_t address, int rqlen)
{
}

static uint32_t
piar_read(void *clientData, uint32_t address, int rqlen)
{
    return 0;
}

static void
piar_write(void *clientData, uint32_t value, uint32_t address, int rqlen)
{
}

static uint32_t
slibxr_read(void *clientData, uint32_t address, int rqlen)
{
    ScintMod *scint = clientData;
    uint8_t idx = address & 0x7f;
    return scint->regSLIR[idx];
}

static void
slibxr_write(void *clientData, uint32_t value, uint32_t address, int rqlen)
{
    ScintMod *scint = clientData;
    uint8_t idx = address & 0x7f;
    if (verify_writable(scint)) {
        return;
    } 
    if (scint->regSLIR[idx] < array_size(scint->irqBIn)) {
        if (scint->regSLIR[idx] != 0) { /* No interrupt selected */
             SigNode_RemoveLink(scint->irqBIn[scint->regSLIR[idx]], scint->irqFwd[idx]);
        }
    }
    if (value >= array_size(scint->irqBIn)) {
        scint->regSLIR[idx] = value & 0xff;
        if (scint->regSLIR[idx] != 0) {
             SigNode_Link(scint->irqBIn[scint->regSLIR[idx]], scint->irqFwd[idx]);
        }
        return; 
    }
}

static uint32_t
slibr_read(void *clientData, uint32_t address, int rqlen)
{
    ScintMod *scint = clientData;
    uint8_t idx = address & 0x7f;
    return scint->regSLIR[idx];
}

static void
slibr_write(void *clientData, uint32_t value, uint32_t address, int rqlen)
{
    ScintMod *scint = clientData;
    uint8_t idx = address & 0x7f;
    if (verify_writable(scint)) {
        return;
    } 
    if (scint->regSLIR[idx] < array_size(scint->irqBIn)) {
        if (scint->regSLIR[idx] != 0) { /* No interrupt selected */
             SigNode_RemoveLink(scint->irqBIn[scint->regSLIR[idx]], scint->irqFwd[idx]);
        }
    }
    if (value >= array_size(scint->irqBIn)) {
        scint->regSLIR[idx] = value & 0xff;
        if (scint->regSLIR[idx] != 0) {
             SigNode_Link(scint->irqBIn[scint->regSLIR[idx]], scint->irqFwd[idx]);
        }
        return; 
    }
}

static uint32_t
sliar_read(void *clientData, uint32_t address, int rqlen)
{
    ScintMod *scint = clientData;
    uint8_t idx = address & 0x7f;
    return scint->regSLIR[idx];
}

static void
sliar_write(void *clientData, uint32_t value, uint32_t address, int rqlen)
{
    ScintMod *scint = clientData;
    uint8_t idx = address & 0x7f;
    if (verify_writable(scint)) {
        return;
    } 
    if (scint->regSLIR[idx] < array_size(scint->irqAIn)) {
        if (scint->regSLIR[idx] != 0) { /* No interrupt selected */
             SigNode_RemoveLink(scint->irqAIn[scint->regSLIR[idx]], scint->irqFwd[idx]);
        }
    }
    if (value >= array_size(scint->irqAIn)) {
        scint->regSLIR[idx] = value & 0xff;
        if (scint->regSLIR[idx] != 0) {
             SigNode_Link(scint->irqAIn[scint->regSLIR[idx]], scint->irqFwd[idx]);
        }
        return; 
    }
}

static uint32_t 
sliprcr_read(void *clientData, uint32_t address, int rqlen)
{
    ScintMod *scint = clientData;
    return scint->regSLIPRCR;
}
static void
sliprcr_write(void *clientData, uint32_t value, uint32_t address, int rqlen)
{
    ScintMod *scint = clientData;
    scint->regSLIPRCR = value & 1;
}

static uint32_t 
selexdr_read(void *clientData, uint32_t address, int rqlen)
{
    return 0;
}
static void
selexdr_write(void *clientData, uint32_t value, uint32_t address, int rqlen)
{
    ScintMod *scint = clientData;
    if (verify_writable(scint)) {
        return;
    } 
}

static void
Scint_UnMap(void *module_owner, uint32_t base, uint32_t mapsize)
{
    int n;
    for (n = 0; n < 12; n++) {
        IOH_Delete8(REG_PIBR(base, n));
    }
    for (n = 0; n < 6; n++) {
        IOH_Delete8(REG_PIAR(base, n));
    }
    IOH_Delete8(REG_PIAR(base, 11));
    for (n = 128; n < 144; n++) {
        IOH_Delete8(REG_SLIBXR(base, n));
    }
    for (n = 144; n < 208; n++) {
         IOH_Delete8(REG_SLIBR(base, n));
    }
    for (n = 208; n < 256; n++) {
         IOH_Delete8(REG_SLIAR(base, n));
    }
    IOH_Delete8(REG_SELEXDR(base));
    IOH_Delete8(REG_SLIPRCR(base));
}

static void
Scint_Map(void *module_owner, uint32_t base, uint32_t mapsize, uint32_t flags)
{
    ScintMod *scint = module_owner;
    int n;
    for (n = 0; n < 12; n++) {
        IOH_New8(REG_PIBR(base, n), pibr_read, pibr_write, scint);
    }
    for (n = 0; n < 6; n++) {
        IOH_New8(REG_PIAR(base, n), piar_read, piar_write, scint);
    }
    IOH_New8(REG_PIAR(base, 11), piar_read, piar_write, scint);
    for (n = 128; n < 144; n++) {
        IOH_New8(REG_SLIBXR(base, n), slibxr_read, slibxr_write, scint);
    }
    for (n = 144; n < 208; n++) {
         IOH_New8(REG_SLIBR(base, n), slibr_read, slibr_write, scint);
    }
    for (n = 208; n < 256; n++) {
         IOH_New8(REG_SLIAR(base, n), sliar_read, sliar_write, scint);
    }
    IOH_New8(REG_SELEXDR(base), selexdr_read, selexdr_write, scint);
    IOH_New8(REG_SLIPRCR(base), sliprcr_read, sliprcr_write, scint);
    
}

static void
RX65Scint_LinkToICU(ScintMod *scint, const char *icuname) 
{
    SigNode *sigICU, *sigScint;
    unsigned int n;
    for (n = 0; n < 128; n++) {
        sigICU = SigNode_Find("%s.irq%u", icuname, n + 128);
        sigScint = scint->irqFwd[n];
        if (!sigICU) {
            fprintf(stderr, "Scint: ICU signal not found\n");
            exit(1);
        }
        if (!sigScint) {
            fprintf(stderr, "Scint: Scint link signal not found\n");
            exit(1);
        }
        SigNode_Link(sigICU, sigScint);
    }
}

BusDevice *
RX65Scint_New(const char *name, const char *icuname)
{
    ScintMod *scint = sg_new(ScintMod);
    unsigned int i;
    scint->name = sg_strdup(name);
    scint->bdev.first_mapping = NULL;
    scint->bdev.Map = Scint_Map;
    scint->bdev.UnMap = Scint_UnMap;
    scint->bdev.owner = scint;
    scint->bdev.hw_flags = MEM_FLAG_READABLE | MEM_FLAG_WRITABLE;
    for (i = 0; i < array_size(scint->irqAIn); i++) {
        scint->irqAIn[i] = SigNode_New("%s.scirqA%u", name, i);
        scint->irqBIn[i] = SigNode_New("%s.scirqB%u", name, i);
        if (!scint->irqAIn[i] || !scint->irqBIn[i]) {
            fprintf(stderr, "Can not create input IRQ line for \"%s\"\n", name);
            exit(1);
        }
    }
    for (i = 0; i < array_size(scint->irqFwd); i++) {
        scint->irqFwd[i] = SigNode_New("%s.scirqFwd%u", name, i + 128);
    }
    RX65Scint_LinkToICU(scint, icuname);
    return &scint->bdev;
}
