/**
 ****************************************************
 * Group interrupts for RX65
 ****************************************************
 */
#include <stdint.h>
#include "bus.h"
#include "sgstring.h"
#include "signode.h"
#include "grpirq_rx65.h"

/* Same base as ICU because this is part of ICU */
#define REG_GRPBE0(base)    ((base) + 0x600) 
#define REG_GRPBL0(base)    ((base) + 0x630) 
#define REG_GRPBL1(base)    ((base) + 0x634) 
#define REG_GRPBL2(base)    ((base) + 0x638) 
#define REG_GRPAL0(base)    ((base) + 0x830) 
#define REG_GRPAL1(base)    ((base) + 0x834) 

#define REG_GENBE0(base)    ((base) + 0x640) 
#define REG_GENBL0(base)    ((base) + 0x670) 
#define REG_GENBL1(base)    ((base) + 0x674) 
#define REG_GENBL2(base)    ((base) + 0x678) 
#define REG_GENAL0(base)    ((base) + 0x870) 
#define REG_GENAL1(base)    ((base) + 0x874) 

#define REG_GCRBE0(base)    ((base) + 0x680)  /* Edge detection interrupts */

#define IDX_BE0     (0)
#define IDX_BL0     (1)
#define IDX_BL1     (2)
#define IDX_BL2     (3)
#define IDX_AL0     (4)
#define IDX_AL1     (5)

typedef struct GrpIrqModule GrpIrqModule;

typedef struct GrpIrq {
    GrpIrqModule *gri;    
    SigNode *sigGrpIrq;
    uint32_t bitNr;
    uint32_t grpNr;
} GrpIrq;

struct GrpIrqModule {
    BusDevice bdev; 
    uint32_t regGRP[6];
    uint32_t regGEN[6];
    SigNode *sigIrqOut[6];
    GrpIrq grpIrq[6 * 32];
};

/**
 *********************************************************************
 * GRPBE0 interrupt is the EDGE group. The interruptcontroller
 * gets the result as an level interrupt.
 *********************************************************************
 */
static void
update_interrupt(GrpIrqModule *gri, unsigned int grpNr)
{
    if (gri->regGRP[grpNr] & gri->regGEN[grpNr]) {
        SigNode_Set(gri->sigIrqOut[grpNr], SIG_LOW);
    } else {
        SigNode_Set(gri->sigIrqOut[grpNr], SIG_HIGH);
    }
}
/**
 *******************************************************************
 * BE0 is the group for EDGE interrupts 
 *******************************************************************
 */
static void
GrpBE0_IrqTraceProc (SigNode *sig, int value, void *eventData)
{
    GrpIrq *grpIrq = eventData;
    GrpIrqModule *gri = grpIrq->gri;
    if (value == SIG_LOW) {
        if (!(gri->regGRP[IDX_BE0] & (1 << grpIrq->bitNr))) {
            gri->regGRP[IDX_BE0] |= (1 << grpIrq->bitNr);
            update_interrupt(gri, IDX_BE0);
        }
    }
}

static void
GrpLvl_IrqTraceProc(SigNode *sig, int value, void *eventData)
{
    GrpIrq *grpIrq = eventData;
    GrpIrqModule *gri = grpIrq->gri;
    unsigned int grpNr = grpIrq->grpNr;
    if (value == SIG_LOW) {
        if (!(gri->regGRP[grpNr] & (1 << grpIrq->bitNr))) {
            gri->regGRP[grpNr] |= (1 << grpIrq->bitNr);
            update_interrupt(gri, grpNr);
        }
    } else if (value == SIG_HIGH) {
        if ((gri->regGRP[grpNr] & (1 << grpIrq->bitNr))) {
            gri->regGRP[grpNr] &= ~(1 << grpIrq->bitNr);
            update_interrupt(gri, grpNr);
        }
    }
}

static uint32_t
grp_read(void *clientData, uint32_t address, int rqlen)
{
    GrpIrq *grpIrq = clientData;
    GrpIrqModule *gri = grpIrq->gri; 
    return gri->regGRP[grpIrq->grpNr];
}

static void
grp_write(void *clientData, uint32_t value, uint32_t address, int rqlen)
{
    fprintf(stderr, "%s: Writing to readonly register\n", __func__);
    return;
}

static uint32_t
gen_read(void *clientData, uint32_t address, int rqlen)
{
    GrpIrq *grpIrq = clientData;
    GrpIrqModule *gri = grpIrq->gri; 
    return gri->regGEN[grpIrq->grpNr];
}

static void
gen_write(void *clientData, uint32_t value, uint32_t address, int rqlen)
{
    GrpIrq *grpIrq = clientData;
    GrpIrqModule *gri = grpIrq->gri; 
    gri->regGEN[grpIrq->grpNr] = value;
    update_interrupt(gri, grpIrq->grpNr);
    return;
}

static uint32_t
gcr_read(void *clientData, uint32_t address, int rqlen)
{
    return 0;
}

static void
gcr_write(void *clientData, uint32_t value, uint32_t address, int rqlen)
{
    GrpIrq *grpIrq = clientData;
    GrpIrqModule *gri = grpIrq->gri; 
    gri->regGRP[grpIrq->grpNr] &= ~value;
    update_interrupt(gri, grpIrq->grpNr);
    return;
}

static void 
GRI_UnMap(void *module_owner, uint32_t base, uint32_t mapsize)
{
    IOH_Delete32(REG_GRPBE0(base));
    IOH_Delete32(REG_GRPBL0(base));
    IOH_Delete32(REG_GRPBL1(base));
    IOH_Delete32(REG_GRPBL2(base));
    IOH_Delete32(REG_GRPAL0(base));
    IOH_Delete32(REG_GRPAL1(base));

    IOH_Delete32(REG_GENBE0(base));
    IOH_Delete32(REG_GENBL0(base));
    IOH_Delete32(REG_GENBL1(base));
    IOH_Delete32(REG_GENBL2(base));
    IOH_Delete32(REG_GENAL0(base));
    IOH_Delete32(REG_GENAL1(base));

    IOH_Delete32(REG_GCRBE0(base));
}

static void 
GRI_Map(void *module_owner, uint32_t base, uint32_t mapsize, uint32_t flags)
{
    GrpIrqModule *gri = module_owner; 
    IOH_New32(REG_GRPBE0(base), grp_read, grp_write, &gri->grpIrq[IDX_BE0 * 32 + 0]);
    IOH_New32(REG_GRPBL0(base), grp_read, grp_write, &gri->grpIrq[IDX_BL0 * 32 + 0]);
    IOH_New32(REG_GRPBL1(base), grp_read, grp_write, &gri->grpIrq[IDX_BL1 * 32 + 0]);
    IOH_New32(REG_GRPBL2(base), grp_read, grp_write, &gri->grpIrq[IDX_BL2 * 32 + 0]);
    IOH_New32(REG_GRPAL0(base), grp_read, grp_write, &gri->grpIrq[IDX_AL0 * 32 + 0]);
    IOH_New32(REG_GRPAL1(base), grp_read, grp_write, &gri->grpIrq[IDX_AL1 * 32 + 0]);

    IOH_New32(REG_GENBE0(base), gen_read, gen_write, &gri->grpIrq[IDX_BE0 * 32 + 0]);
    IOH_New32(REG_GENBL0(base), gen_read, gen_write, &gri->grpIrq[IDX_BL0 * 32 + 0]);
    IOH_New32(REG_GENBL1(base), gen_read, gen_write, &gri->grpIrq[IDX_BL1 * 32 + 0]);
    IOH_New32(REG_GENBL2(base), gen_read, gen_write, &gri->grpIrq[IDX_BL2 * 32 + 0]);
    IOH_New32(REG_GENAL0(base), gen_read, gen_write, &gri->grpIrq[IDX_AL0 * 32 + 0]);
    IOH_New32(REG_GENAL1(base), gen_read, gen_write, &gri->grpIrq[IDX_AL1 * 32 + 0]);

    IOH_New32(REG_GCRBE0(base), gcr_read, gcr_write, &gri->grpIrq[IDX_BE0 * 32 + 0]);
}

BusDevice *
RX65GrpIrqs_New(const char *name) 
{
    int i;
    GrpIrqModule *gri = sg_new(GrpIrqModule);
    gri->bdev.first_mapping = NULL;
    gri->bdev.Map = GRI_Map;
    gri->bdev.UnMap = GRI_UnMap;
    gri->bdev.owner = gri;
    gri->bdev.hw_flags = MEM_FLAG_READABLE | MEM_FLAG_WRITABLE;

    gri->sigIrqOut[IDX_BE0] = SigNode_New("%s.grpBE0.irqOut", name);
    gri->sigIrqOut[IDX_BL0] = SigNode_New("%s.grpBL0.irqOut", name);
    gri->sigIrqOut[IDX_BL1] = SigNode_New("%s.grpBL1.irqOut", name);
    gri->sigIrqOut[IDX_BL2] = SigNode_New("%s.grpBL2.irqOut", name);
    gri->sigIrqOut[IDX_AL0] = SigNode_New("%s.grpAL0.irqOut", name);
    gri->sigIrqOut[IDX_AL1] = SigNode_New("%s.grpAL1.irqOut", name);

    for (i = 0; i < 32; i++) {
        GrpIrq *grpIrq;

        grpIrq = &gri->grpIrq[IDX_BE0 * 32 + i];
        grpIrq->grpNr = IDX_BE0;
        grpIrq->gri = gri; 
        grpIrq->sigGrpIrq = SigNode_New("%s.grpBE0.irq%u", name, i);
        grpIrq->bitNr = i;
        SigNode_Trace(grpIrq->sigGrpIrq, GrpBE0_IrqTraceProc, grpIrq);

        grpIrq = &gri->grpIrq[IDX_BL0 * 32 + i];
        grpIrq->grpNr = IDX_BL0;
        grpIrq->gri = gri; 
        grpIrq->sigGrpIrq = SigNode_New("%s.grpBL0.irq%u", name, i);
        grpIrq->bitNr = i;
        SigNode_Trace(grpIrq->sigGrpIrq, GrpLvl_IrqTraceProc, grpIrq);

        grpIrq = &gri->grpIrq[IDX_BL1 * 32 + i];
        grpIrq->grpNr = IDX_BL1;
        grpIrq->gri = gri; 
        grpIrq->sigGrpIrq = SigNode_New("%s.grpBL1.irq%u", name, i);
        grpIrq->bitNr = i;
        SigNode_Trace(grpIrq->sigGrpIrq, GrpLvl_IrqTraceProc, grpIrq);

        grpIrq = &gri->grpIrq[IDX_BL2 * 32 + i];
        grpIrq->grpNr = IDX_BL2;
        grpIrq->gri = gri; 
        grpIrq->sigGrpIrq = SigNode_New("%s.grpBL2.irq%u", name, i);
        grpIrq->bitNr = i;
        SigNode_Trace(grpIrq->sigGrpIrq, GrpLvl_IrqTraceProc, grpIrq);

        grpIrq = &gri->grpIrq[IDX_AL0 * 32 + i];
        grpIrq->grpNr = IDX_AL0;
        grpIrq->gri = gri; 
        grpIrq->sigGrpIrq = SigNode_New("%s.grpAL0.irq%u", name, i);
        grpIrq->bitNr = i;
        SigNode_Trace(grpIrq->sigGrpIrq, GrpLvl_IrqTraceProc, grpIrq);

        grpIrq = &gri->grpIrq[IDX_AL1 * 32 + i];
        grpIrq->grpNr = IDX_AL1;
        grpIrq->gri = gri; 
        grpIrq->sigGrpIrq = SigNode_New("%s.grpAL1.irq%u", name, i);
        grpIrq->bitNr = i;
        SigNode_Trace(grpIrq->sigGrpIrq, GrpLvl_IrqTraceProc, grpIrq);
    }
    return &gri->bdev;
}
