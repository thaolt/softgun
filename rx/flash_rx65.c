/**
 ***************************************************************************************************
 * RX65-Flash
 *      Emulation of the internal flash of Renesas RX65  series
 *
 * Status: Error handling is bad, lock bits not implemented
 *
 * Copyright 2012 Jochen Karrer. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are
 * permitted provided that the following conditions are met:
 *
 *   1. Redistributions of source code must retain the above copyright notice, this list of
 *       conditions and the following disclaimer.
 *
 *    2. Redistributions in binary form must reproduce the above copyright notice, this list
 *       of conditions and the following disclaimer in the documentation and/or other materials
 *       provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY Jochen Karrer ``AS IS'' AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 * FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * The views and conclusions contained in the software and documentation are those of the
 * authors and should not be interpreted as representing official policies, either expressed
 * or implied, of Jochen Karrer.
 ***************************************************************************************************
 */

#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <ctype.h>

#include "flash_rx65.h"
#include "configfile.h"
#include "cycletimer.h"
#include "bus.h"
#include "signode.h"
#include "diskimage.h"
#include "sgstring.h"
#include "clock.h"

#define REG_FWEPROR(base)       (0x0008C296)
#define     REG_FWEPROR_FLWE_MSK    (3 << 0)
#define REG_FASTAT(base)              (0x007FE010)
#define     FASTAT_CFAE     (1 << 7)
#define     FASTAT_CMDLK    (1 << 4)
#define     FASTAT_DFAE     (1 << 3)
#define REG_FAEINT(base)              (0x007FE014)
#define     FAEINT_CFAEIE   (1 << 7) 
#define     FAEINT_CMDLKIE  (1 << 4)
#define     FAEINT_DFAEIE   (1 << 3)
#define REG_FRDYIE(base)              (0x007FE018)
#define     FRDYIE_FRDYIE   (1 << 0)
#define REG_FSADDR(base)              (0x007FE030)
#define REG_FEADDR(base)              (0x007FE034)  
#define REG_FSTATR              (0x007FE080)
#define     FSTATR_ILGCOMERR    (1 << 23)
#define     FSTATR_FESETERR     (1 << 22)
#define     FSTATR_SECERR       (1 << 21)
#define     FSTATR_OTERR        (1 << 20)
#define     FSTATR_FRDY         (1 << 15)
#define     FSTATR_ILGLERR      (1 << 14)
#define     FSTATR_ERSERR       (1 << 13)
#define     FSTATR_PRGERR       (1 << 12)
#define     FSTATR_SUSRDY       (1 << 11)
#define     FSTATR_DBFUL        (1 << 10)
#define     FSTATR_ERSSPD       (1 << 9)
#define     FSTATR_PRGSPD       (1 << 8)
#define     FSTATR_FLWEERR      (1 << 6)
#define REG_FENTRYR(base)             (0x007FE084)
#define     FENTRYR_KEY_MSK     (0xff << 8)
#define     FENTRYR_FETNRYD     (1 << 7)
#define     FENTRYR_FENTRYC     (1 << 0)
#define REG_FSUINITR(base)            (0x007FE08C)
#define     FSUINITR_KEY_MSK    (0xff << 8)
#define     FSUINITR_SUINIT     (1 << 0)
#define REG_FCMDR(base)               (0x007FE0A0)
#define     FCMDR_CMDR_MSK      (0xff << 8)
#define     FCMDR_PCMDR_MSK     (0xff)
#define REG_FBCCNT(base)              (0x007FE0D0)
#define     FBCNNT_BCDIR        (1 << 0)
#define REG_FBCSTAT(base)             (0x007FE0D4)
#define     FBCSTAT_BCST        (1 << 0)
#define REG_FPSADDR(base)             (0x007FE0D8)
#define REG_FAWMON(base)              (0x007FE0DC)
#define     FAWMON_FAWS_MSK     (0xfff << 0)
#define     FAWMON_FAWE_MSK     (0xfff << 16)
#define     FAWMON_BTFLG        (1U << 31)
#define     FAWMON_FSPR         (1U << 15)
#define REG_FCPSR(base)               (0x007FE0E0)
#define     FCPSR_ESUSPMD       (1 << 0)
#define REG_FPCKAR(base)              (0x007FE0E4)
#define     FPCKAR_PCKA         (0xff << 0)
#define     FPCKAR_KEY_MSK      (0xff << 8)
#define REG_FSUACR(base)              (0x007FE0E8)

typedef struct RX65Flash {
    BusDevice bdev;
    uint32_t romsize;
    uint32_t dfsize;
    uint32_t blank_size;

    DiskImage *rom_image;
    DiskImage *data_image;
    DiskImage *blank_image;
    //DiskImage *fcu_rom_image;

    uint8_t *rom_mem;
    uint8_t *data_mem;
    uint8_t *blank_mem;
    bool dbgMode;

    /* The registers */
    uint8_t regFWEPROR;
    uint8_t regFASTAT;
    uint8_t regFAEINT;
    uint8_t regFRDYIE;
    uint32_t regFSADDR;
    uint32_t regFEADDR;
    uint32_t regFSTATR;
    uint16_t regFENTRYR;
    uint16_t regFSUINITR;
    uint16_t regFCMDR;
    uint8_t regFBCCNT;
    uint8_t regFBCSTAT;
    uint32_t regFPSADDR;
    uint32_t regFAWMON;
    uint16_t regFCPSR;
    uint16_t regFPCKAR;
    uint16_t regFSUACR;
} RX65Flash;

static uint32_t
parse_memsize(char *str)
{
    uint32_t size;
    unsigned char c;
    if (sscanf(str, "%d", &size) != 1) {
        return 0;
    }
    if (sscanf(str, "%d%c", &size, &c) == 1) {
        return size;
    }
    switch (c) {
        case 'M':
            return size * 1024 * 1024;
        case 'k':
            return size * 1024;
    }
    return 0;
}

/**
 *****************************************************************************
 * \fn BusDevice * RX65Flash_New(const char *flash_name)
 *****************************************************************************
 */
BusDevice *
RX65Flash_New(const char *flash_name)
{
    RX65Flash *rxf = sg_new(RX65Flash);
    char *imagedir;
    char *romsizestr;
    char *datasizestr;
    uint32_t debugmode = 0;
    imagedir = Config_ReadVar("global", "imagedir");
    romsizestr = Config_ReadVar(flash_name, "romsize");
    datasizestr = Config_ReadVar(flash_name, "datasize");
    if (romsizestr) {
        rxf->romsize = parse_memsize(romsizestr);
        if (rxf->romsize == 0) {
            fprintf(stderr, "RX65 Flash \"%s\" has zero size\n", flash_name);
            return NULL;
        }
    } else {
        fprintf(stderr, "Flash size for CPU is not configured\n");
        return NULL;
    }
    if (datasizestr) {
        rxf->dfsize = parse_memsize(datasizestr);
#if 0
        if (rxf->dfsize == 0) {
            fprintf(stderr, "RX65 Flash \"%s\" has zero size\n", flash_name);
            return NULL;
        }
#endif
        rxf->blank_size = rxf->dfsize >> 4; /* One blank status bit for every two Bytes of data */
    } else {
        fprintf(stderr, "Flash size for CPU Data Flash is not configured\n");
        return NULL;
    }
    Config_ReadUInt32(&debugmode, flash_name, "debug");
    rxf->dbgMode = debugmode;
    if (imagedir) {
        char *mapfile = alloca(strlen(imagedir) + strlen(flash_name) + 20);
        sprintf(mapfile, "%s/%s_rom.img", imagedir, flash_name);
        rxf->rom_image =
            DiskImage_Open(mapfile, rxf->romsize, DI_RDWR | DI_CREAT_FF);
        if (!rxf->rom_image) {
            fprintf(stderr, "RX-Flash: Open disk image for ROM failed\n");
            exit(1);
        }
        rxf->rom_mem = DiskImage_Mmap(rxf->rom_image);
        if (rxf->dfsize) {
            sprintf(mapfile, "%s/%s_data.img", imagedir, flash_name);
            rxf->data_image =
                DiskImage_Open(mapfile, rxf->dfsize, DI_RDWR | DI_CREAT_00);
            if (!rxf->data_image) {
                fprintf(stderr, "RX-Flash: Open disk image for Data Flash failed\n");
                exit(1);
            }
            rxf->data_mem = DiskImage_Mmap(rxf->data_image);
        }
#if 0

        sprintf(mapfile, "%s/%s_blank.img", imagedir, flash_name);
        rxf->blank_image =
            DiskImage_Open(mapfile, rxf->blank_size, DI_RDWR | DI_CREAT_FF);
        if (!rxf->blank_image) {
            fprintf(stderr,
                "RX-Flash: Open disk image for Blank Check Bitmap failed\n");
            exit(1);
        }
        rxf->blank_mem = DiskImage_Mmap(rxf->blank_image);
#endif

#if 0
        sprintf(mapfile, "%s/%s_fcu_rom.img", imagedir, flash_name);
        rxf->fcu_rom_image = DiskImage_Open(mapfile, FCU_ROM_SIZE, DI_RDONLY);
        if (!rxf->fcu_rom_image) {
            unsigned int i;
            fprintf(stderr, "RX-Flash: Open disk image \"%s\" for FCU ROM failed\n",
                mapfile);
            rxf->fcu_rom = sg_calloc(FCU_ROM_SIZE);
            for (i = 0; i < FCU_ROM_SIZE; i++) {
                rxf->fcu_rom[i] = rand();
            }
        } else {
            rxf->fcu_rom = DiskImage_Mmap(rxf->fcu_rom_image);
            if (!rxf->fcu_rom) {
                fprintf(stderr, "MMap of FCU rom failed\n");
                exit(1);
            }
        }
#endif
        fprintf(stderr, "Mapped ROM    to %p\n", rxf->rom_mem);
        fprintf(stderr, "Mapped DFLASH to %p\n", rxf->data_mem);
        fprintf(stderr, "Mapped BLANK  to %p\n", rxf->blank_mem);
    } else {
        rxf->rom_mem = sg_calloc(rxf->romsize);
        memset(rxf->rom_mem, 0xff, rxf->romsize);
        rxf->data_mem = sg_calloc(rxf->dfsize);
        memset(rxf->data_mem, 0x00, rxf->dfsize);   /* ?? */
        rxf->blank_mem = sg_calloc(rxf->blank_size);
        memset(rxf->blank_mem, 0xff, rxf->blank_size);
    }
    return &rxf->bdev;
}
