/*
 *************************************************************
 * Port Output Enable 2 Module as defined in RX111 Manual
 *************************************************************
 */
#include <stdint.h>
#include "bus.h"
#include "clock.h"
#include "sgstring.h"
#include "signode.h"

#define REG_POE_ICSR1(base)     ((base) + 0x00)
#define REG_POE_OCSR1(base)     ((base) + 0x02) 
#define REG_POE_ICSR2(base)     ((base) + 0x08)
#define REG_POE_SPOER(base)     ((base) + 0x0a)
#define REG_POE_POECR1(base)    ((base) + 0x0b)
#define REG_POE_POECR2(base)    ((base) + 0x0c)
#define REG_POE_ICSR3(base)     ((base) + 0x0e)

typedef struct POE2a {
    BusDevice bdev;
    uint16_t regICSR1;
    uint16_t regOCSR1;
    uint16_t regICSR2;
    uint8_t  regSPOER;
    uint8_t  regPOECR1;
    uint8_t  regPOECR2;
    uint16_t regICSR3; 
} POE2a;

/**
 *************************************************************************************
 * Contains mode select bitfields and flags (indicator) if high impedance
 * request has happened.
 * PIEI enables an interrupt when any of the POE flags  POE0F-3F is set 
 *************************************************************************************
 */
static uint32_t
icsr1_read(void *clientData, uint32_t address, int rqlen)
{
    fprintf(stderr, "%s not implemented\n", __func__);
    return 0; 
}

static void
icsr1_write(void *clientData, uint32_t value, uint32_t address, int rqlen)
{
    fprintf(stderr, "%s not implemented\n", __func__);
}

static uint32_t
ocsr1_read(void *clientData, uint32_t address, int rqlen)
{
    fprintf(stderr, "%s not implemented\n", __func__);
    return 0; 
}

static void
ocsr1_write(void *clientData, uint32_t value, uint32_t address, int rqlen)
{
    fprintf(stderr, "%s not implemented\n", __func__);
}

static uint32_t
icsr2_read(void *clientData, uint32_t address, int rqlen)
{
    fprintf(stderr, "%s not implemented\n", __func__);
    return 0; 
}

static void
icsr2_write(void *clientData, uint32_t value, uint32_t address, int rqlen)
{
    fprintf(stderr, "%s not implemented\n", __func__);
}

static uint32_t
spoer_read(void *clientData, uint32_t address, int rqlen)
{
    fprintf(stderr, "%s not implemented\n", __func__);
    return 0; 
}

static void
spoer_write(void *clientData, uint32_t value, uint32_t address, int rqlen)
{
    fprintf(stderr, "%s not implemented\n", __func__);
}

static uint32_t
poecr1_read(void *clientData, uint32_t address, int rqlen)
{
    fprintf(stderr, "%s not implemented\n", __func__);
    return 0; 
}

static void
poecr1_write(void *clientData, uint32_t value, uint32_t address, int rqlen)
{
    fprintf(stderr, "%s not implemented\n", __func__);
}

static uint32_t
poecr2_read(void *clientData, uint32_t address, int rqlen)
{
    fprintf(stderr, "%s not implemented\n", __func__);
    return 0; 
}

static void
poecr2_write(void *clientData, uint32_t value, uint32_t address, int rqlen)
{
    fprintf(stderr, "%s not implemented\n", __func__);
}

static uint32_t
icsr3_read(void *clientData, uint32_t address, int rqlen)
{
    fprintf(stderr, "%s not implemented\n", __func__);
    return 0; 
}

static void
icsr3_write(void *clientData, uint32_t value, uint32_t address, int rqlen)
{
    fprintf(stderr, "%s not implemented\n", __func__);
}

static void
POE_UnMap(void *module_owner, uint32_t base, uint32_t mapsize)
{
    IOH_Delete16(REG_POE_ICSR1(base));
    IOH_Delete16(REG_POE_OCSR1(base));
    IOH_Delete16(REG_POE_ICSR2(base));
    IOH_Delete8(REG_POE_SPOER(base)); 
    IOH_Delete8(REG_POE_POECR1(base));
    IOH_Delete8(REG_POE_POECR2(base));
    IOH_Delete16(REG_POE_ICSR3(base)); 
}

static void
POE_Map(void *module_owner, uint32_t base, uint32_t mapsize, uint32_t flags)
{
    POE2a *poe = module_owner;
    IOH_New16(REG_POE_ICSR1(base), icsr1_read, icsr1_write, poe);
    IOH_New16(REG_POE_OCSR1(base), ocsr1_read, ocsr1_write, poe);
    IOH_New16(REG_POE_ICSR2(base), icsr2_read, icsr2_write, poe);
    IOH_New8(REG_POE_SPOER(base), spoer_read, spoer_write, poe); 
    IOH_New8(REG_POE_POECR1(base), poecr1_read, poecr1_write, poe);
    IOH_New8(REG_POE_POECR2(base), poecr2_read, poecr2_write, poe);
    IOH_New16(REG_POE_ICSR3(base), icsr3_read, icsr3_write, poe); 
}

BusDevice *
RxPOE2A_New(const char *name)
{
    POE2a *poe = sg_new(POE2a);
    poe->bdev.first_mapping = NULL;
    poe->bdev.Map = POE_Map;
    poe->bdev.UnMap = POE_UnMap;
    poe->bdev.owner = poe;
    poe->bdev.hw_flags = MEM_FLAG_READABLE | MEM_FLAG_WRITABLE;
    return &poe->bdev;
}

